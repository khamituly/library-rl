<%@ page import="java.io.File" %>
<%@ page import="Models.Book" %>
<%@ page import="java.util.List" %>
<%@ page import="Models.User" %>
<%@ page import="javax.websocket.Session" %>
<%@ page import="Repository.BookRepo" %>
<%@ page import="Models.Genre" %><%--
  Created by IntelliJ IDEA.
  User: khamituly
  Date: 15.02.2021
  Time: 14:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<style>
    * {
        margin: 0;
        padding: 0;
        background: whitesmoke;
        font-family: sans-serif;
    }

    .thisPage{
        display: flex;
        flex-direction: column;
    }

    .files{
        margin-top: 10%;
        margin-left: 5%;
    }

    .search-box {
        position: absolute;
        top: 17%;
        left: 50%;
        transform: translate(-50%, -50%);
        background: black;
        height: 50px;
        border-radius: 50px;
        padding: 10px;
    }

    .search-btn {
        color: white;
        float: right;
        width: 50px;
        height: 50px;
        border-radius: 50%;
        background: black;
        display: flex;
        justify-content: center;
        align-items: center;
        transition: 2s;
    }

    .fa-search {
        background: black;
        font-size: 24px;
    }

    .search-input {
        border: none;
        background: none;
        outline: none;
        float: left;
        padding: 0;
        color: white;
        font-size: 20px;
        transition: 0.4s;
        line-height: 50px;
        width: 0;
    }

    .search-box:hover > .search-input {
        width: 250px;
        padding: 0 7px;
    }


    a {
        text-decoration: none;
    }

    .item {
        min-height: 86px;
        width: 118px;
        position: relative;
        z-index: 1;
        background-repeat: no-repeat;
        background-size: 100%;
        display: inline-block;
        margin: 2rem;
    }
    .item.folder {
        background-image: url("https://static2.sharepointonline.com/files/fabric/office-ui-fabric-react-assets/foldericons/folder-large_backplate.svg");
    }
    .item.folder:before {
        background-image: url("https://static2.sharepointonline.com/files/fabric/office-ui-fabric-react-assets/foldericons/folder-large_frontplate_nopreview.svg");
        position: absolute;
        content: attr(data-count);
        font-size: 14px;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        z-index: 1;
        background-size: 100%;
        color: #fff;
        padding-right: 12px;
        padding-bottom: 10px;
        box-sizing: border-box;
        display: flex;
        align-items: flex-end;
        flex-direction: row-reverse;
    }
    .item.folder:after {
        position: absolute;
        background-color: #ffffff;
        content: "";
        top: 7px;
        bottom: 7px;
        left: 7px;
        right: 7px;
        opacity: 1;
        -webkit-transition: opacity 0.2s linear;
        transition: opacity 0.2s linear;
        -webkit-box-shadow: 0 1px 3px 2px rgba(1, 1, 0, 0.2);
        box-shadow: 0 1px 3px 2px rgba(1, 1, 0, 0.2);
    }
    .item.folder.empty:after,
    .item.folder.empty .number {
        display: none;
    }
    .item .name {
        color: #333333;
        position: absolute;
        bottom: -1.75rem;
        left: 50%;
        transform: translateX(-50%);
    }
</style>
<body>
<%
    Boolean isExist = false;
     User user = null;
    if(request.getSession().getAttribute("user")!= null){
        user = (User) request.getSession().getAttribute("user");
        isExist = true;
    }

    Boolean isAdmin = false;

    if(isExist) {
        isExist = true;
        if (user.getRoleId() == 1) {
            isAdmin = true;
        }
    }
    BookRepo bookRepo = new BookRepo();
    List<Book> list = bookRepo.getAllBooks();
    List<Genre> genres = bookRepo.getAllGenre();
%>

<center>

    <form action="<%=request.getContextPath()%>/search" method="post">
        <input type="text" placeholder="search book by name" name="name" ><br>
             Find by genre
            <select name = "genre">
                <option value="0"></option>
                <%
                    for(Genre genre : genres){
                %>
               <option value="<%=genre.getId()%>"><%=genre.getName()%>  </option>

                <%}%>
                <input type="submit" value="search" >
            </select>

    </form>
    <div class="files">
        <%for (Book book : list){%>
        <a href="<%=request.getContextPath()%>/book?id=<%=book.getId()%>">
            <div class="item folder">
                <span class="name"><%=book.getName()%></span>
            </div>
        </a>
        <%if(isExist){%>
        <form action="<%=request.getContextPath()%>/download" method="post">
            <input type="submit" value="download">
            <input type="number" hidden name="id" value="<%=book.getId()%>">
        </form>
        <%}%>
        <%if(isAdmin){%>
        <form action="<%=request.getContextPath()%>/books" method="post">
            <input type="hidden" name="choice" value="remove">
            <input type="submit" value="remove">
            <input type="number" hidden name="id" value="<%=book.getId()%>">
        </form>
        <%}%>
        <%}%>
    </div>

<%-- this is login for guest --%>
    <%
    if(!isExist){
    %>
    <a href="login.jsp">Login</a>
    <a href="signIn.jsp">Sig In</a>
    <%}else{%>
    <a href="profileUser.jsp">Profile</a>
    <%}%>
</center>
</body>
</html>
