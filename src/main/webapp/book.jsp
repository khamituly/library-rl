
<%@ page import="Models.Book" %>
<%@ page import="Models.User" %>
<%@ page import="Models.Genre" %>
<%@ page import="Repository.BookRepo" %><%--
  Created by IntelliJ IDEA.
  User: khamituly
  Date: 15.02.2021
  Time: 12:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
    <%
        Boolean isExist = false;
        User user = null;
        if(request.getSession().getAttribute("user")!= null){
            user = (User) request.getSession().getAttribute("user");
            isExist = true;
        }

        Boolean isAdmin = false;

        if(isExist) {
            isExist = true;
            if (user.getRoleId() == 1) {
                isAdmin = true;
            }
        }
        Book book = (Book) request.getAttribute("book");
        Genre genre = (Genre) request.getAttribute("genre");
    %>

    <div style="margin: auto; width: 60%; border: 3px solid black; padding: 10px;">
        <table style="border-collapse: collapse; border: 1px solid black; width: 100%"/>
        <tr>
            <td style="width:40%" rowspan="6"><img src="<%=book.getImage()%>" width="200px" height="300px" /></td>
        </tr>
        <tr><td><h2 ><%=book.getName()%></h2></td></tr>
        <tr><td><h4 >Жанр: <%=genre.getName()%></h4></td></tr>
        <tr><td><span  type="text" name="description" ><%=book.getDescription()%></span></td></tr>
        </table>
        <br>
        <br>
        <%if(isExist){%>
        <form action="<%=request.getContextPath()%>/download" method="post">
            <input type="submit" value="download">
            <input type="number" hidden name="id" value="<%=book.getId()%>">
        </form>
        <%}%>
        <%if(isAdmin){%>
        <form action="<%=request.getContextPath()%>/books" method="post">
            <input type="hidden" name="choice" value="remove">
            <input type="submit" value="remove">
            <input type="number" hidden name="id" value="<%=book.getId()%>">
        </form>
        <%}%>
    </div>
</body>
</html>
