<%@ page import="Models.User" %><%--
  Created by IntelliJ IDEA.
  User: khamituly
  Date: 11.02.2021
  Time: 13:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    Boolean isExist = false;
    User user = null;
    if(request.getSession().getAttribute("user")!= null){
        user = (User) request.getSession().getAttribute("user");
        isExist = true;
    }

    Boolean isAdmin = false;

    if(isExist) {
        isExist = true;
        if (user.getRoleId() == 1) {
            isAdmin = true;
        }
    }
%>
<h2>Добро пожаловать <%=user.getName()%> !</h2>
    <form action="<%=request.getContextPath()%>/books" method="get">
        <input type="hidden" name="choice" value="view">
        <input type="submit" value="Books">
    </form>


    <%if(isAdmin){%>
    <form action="<%=request.getContextPath()%>/books" method="get">
        <input type="hidden" name="choice" value="upload">
        <input type="submit" value="upload book">
    </form>
    <%}%>


    <form action="<%=request.getContextPath()%>/logout" method="post">
        <input type="hidden" name="id" value="<%=user.getId()%>">
        <input type="submit" value="logout">
    </form>

</body>
</html>
