<%@ page import="java.util.List" %>
<%@ page import="Models.Genre" %><%--
  Created by IntelliJ IDEA.
  User: khamituly
  Date: 20.02.2021
  Time: 16:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<%
    List<Genre> genres = (List<Genre>) request.getAttribute("list");
%>
<body>
<form action="<%=request.getContextPath()%>/upload" method="get" enctype="multipart/form-data">
    <input type="text" name="fileName" placeholder="file name"><br>
    <input type="textarea" size="150" name="description" placeholder="short description"><br>
    <input type="text" name="image" placeholder="image url"><br>
    <select name = "genre">
        <%
        for(Genre genre : genres){
        %>
        <option value="<%=genre.getId()%>"><%=genre.getName()%></option>
        <%}%>
    </select>
    <input type="submit">
</form>
</body>
</html>
