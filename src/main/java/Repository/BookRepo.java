package Repository;

import Models.Book;
import Models.DB;
import Models.Genre;


import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookRepo {
    private final DB db;
    private final Connection connection;

    public BookRepo() {
        this.db = new DB();
        this.connection = db.getConnection();

    }

    public List<Book> getBookByName(String name) throws SQLException {
        List<Book> list = new ArrayList<>();
        String str1 = "%"+name;
        String str2 = "%"+name+"%";
        String str3 = name+"%";

        String SQL = "SELECT * FROM books WHERE name = ? OR name LIKE ? OR name LIKE ?";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setString(1,str1);
        stm.setString(2,str2);
        stm.setString(3,str3);
        ResultSet res = stm.executeQuery();
        Book book = null;
        while (res.next()) {

            list.add( new Book(
                            res.getInt("id"),
                            res.getString("name"),
                            res.getString("description"),
                            res.getString("image"),
                            res.getString("filePath"),
                            res.getInt("genreId")
            ));
        }
        return list;
    }

    public void addBook(Book book) throws SQLException {
        String SQL = "INSERT INTO books(name,description,image,filePath,genreId) VALUES(?,?,?,?,?)";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setString(1, book.getName());
        stm.setString(2, book.getDescription());
        stm.setString(3, book.getImage());
        stm.setString(4, book.getFilePath());
        stm.setInt(5,book.getGenreId());
        stm.executeUpdate();
    }

    public List<Book> getAllBooks() throws SQLException {

        String SQL = "SELECT*FROM books";
        PreparedStatement stm = connection.prepareStatement(SQL);
        ResultSet res = stm.executeQuery();

        List<Book> list = new ArrayList<>();

        while (res.next()) {
            list.add(
                    new Book(
                            res.getInt("id"),
                            res.getString("name"),
                            res.getString("description"),
                            res.getString("image"),
                            res.getString("filePath"),
                            res.getInt("genreId")
                    )
            );
        }

        return list;
    }

    public void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if(files!=null) { //some JVMs return null for empty dirs
            for(File f: files) {
                if(f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
    }

    public void removeBookById(int id) throws SQLException {
        Book book = getBookById(id);
        deleteFolder(new File(book.getFilePath()));
        String SQL = "DELETE FROM books WHERE id  = ?";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setInt(1,id);
        stm.executeUpdate();
    }

    public List<Genre> getAllGenre() throws SQLException {
        String SQL = "SELECT * FROM genres ";
        PreparedStatement stm = connection.prepareStatement(SQL);
        List<Genre> list = new ArrayList<>();
        ResultSet res = stm.executeQuery();
        while (res.next()){
            list.add(
                    new Genre(
                            res.getInt("genreId"),
                            res.getString("genre")
                    )
            );
        }
        return list;
    }

    public Genre getGenreById(int id) throws SQLException {
        String SQL = "SELECT * FROM genres WHERE genreId = ?";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setInt(1,id);
        Genre genre = null;
        ResultSet res = stm.executeQuery();
        while (res.next()){
            genre = new Genre(
                            res.getInt("genreId"),
                            res.getString("genre")
                    );
        }
        return genre;
    }

    public Book getBookById(int id) throws SQLException {
        String SQL = "SELECT*FROM books WHERE id =?";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setInt(1,id);
        ResultSet res = stm.executeQuery();
        Book book = null;
        while (res.next()){
            book = new Book(
                    res.getInt("id"),
                    res.getString("name"),
                    res.getString("description"),
                    res.getString("image"),
                    res.getString("filePath"),
                    res.getInt("genreId"));
        }
        return book;
    }

    public List<Book> getBooksByGenreId(int id) throws SQLException {
        String SQL = "SELECT * FROM books WHERE genreId = ?";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setInt(1,id);
        ResultSet res = stm.executeQuery();
        List<Book> list = new ArrayList<>();
        while (res.next()){
            list.add(new Book(
                    res.getInt("id"),
                    res.getString("name"),
                    res.getString("description"),
                    res.getString("image"),
                    res.getString("filePath"),
                    res.getInt("genreId")
            ));
        }
        return list;
    }

    public List<Book> getBookByGenreAndName(String name,int genreId) throws SQLException {
        String SQL = "SELECT * FROM books WHERE name  LIKE ? AND genreId = ? OR name LIKE ? AND genreId =? OR name LIKE ? AND genreId = ? ";
        PreparedStatement stm = connection.prepareStatement(SQL);
        String str1 = "%"+name;
        String str2 = "%"+name+"%";
        String str3 = name+"%";

        stm.setString(1,str1);
        stm.setInt(2,genreId);
        stm.setString(3,str2);
        stm.setInt(4,genreId);
        stm.setString(5,str3);
        stm.setInt(6,genreId);

        ResultSet res = stm.executeQuery();
        List<Book> list = new ArrayList<>();
        while (res.next()){
            list.add(
                    new Book(
                            res.getInt("id"),
                            res.getString("name"),
                            res.getString("description"),
                            res.getString("image"),
                            res.getString("filePath"),
                            res.getInt("genreId")
                    )
            );
        }
        return list;
    }
}
