package Repository;

import Models.DB;
import Models.User;

import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepo {
    private final DB db;
    private final Connection connection;

    public UserRepo() {
        this.db = new DB();
        this.connection = db.getConnection();
    }

    public User autheticateUser(String email, String passwrod) throws SQLException {
        User user = getUserByEmail(email);
        if(user.getPassword().equals(passwrod)){
            return user;
        }else{
            return null;
        }
    }

    public User getUserByEmail(String email) throws SQLException {
        String SQL = "SELECT*FROM users where email = ?";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setString(1,email);
        User user = null;
        ResultSet res = stm.executeQuery();
        while (res.next()){
            user = new User(
                    res.getInt("id"),
                    res.getString("name"),
                    res.getString("surname"),
                    res.getString("email"),
                    res.getString("password"),
                    res.getInt("roleId")
            );
        }
        return user;
    }

    public List<User> getUsers() throws SQLException {
        String SQL = "SELECT*FROM users ";
        PreparedStatement stm = connection.prepareStatement(SQL);
        List<User> users = new ArrayList<>();
        ResultSet res = stm.executeQuery();
        while (res.next()){
            users.add(new User(
                    res.getInt("id"),
                    res.getString("name"),
                    res.getString("surname"),
                    res.getString("email"),
                    res.getString("password"),
                    res.getInt("roleId")
            ));
        }
        return users;
    }

    public void removeUserById(int id){

    }

    public void updateUser(User user){

    }

    public void addUser(User user) throws SQLException {
        String SQL = "INSERT INTO users(name,surname,email,password,roleId) VALUES(?,?,?,?,?)";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setString(1,user.getName());
        stm.setString(2,user.getSurname());
        stm.setString(3,user.getEmail());
        stm.setString(4,user.getPassword());
        stm.setInt(5, user.getRoleId());
        stm.executeUpdate();
    }
}
