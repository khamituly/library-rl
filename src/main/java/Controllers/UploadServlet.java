package Controllers;

import Models.Book;
import Repository.BookRepo;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

//upload file to the server
@WebServlet(name = "UploadServlet")
public class UploadServlet extends HttpServlet {
    private BookRepo bookRepo = new BookRepo();
    private String path = "C:\\db\\newBook";
    private File dir = new File(path);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            ServletFileUpload sf = new ServletFileUpload(new DiskFileItemFactory());
            List<FileItem> multifiles = sf.parseRequest(request);

            for (FileItem item : multifiles) {
                item.write(new File("C:\\db\\newBook\\" + item.getName()));
            }
            request.setAttribute("list",bookRepo.getAllGenre());
            request.getRequestDispatcher("/uploadDetails.jsp").forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        File file = dir.listFiles()[0];
        String name = file.getName();
        String fileName = request.getParameter("fileName");
        String description = request.getParameter("description");
        String image = request.getParameter("image");
        int genreId = Integer.parseInt(request.getParameter("genre"));
        bookRepo.deleteFolder(file);
        System.out.println(name);

      if(file.renameTo(new File("C:\\db\\Books\\" + name))){
          System.out.println("moved");
      }else{
          System.out.println("doesnt moved");
      }

        System.out.println("before: "+file.getAbsolutePath());

        Book book = new Book(fileName, description, image, "C:\\db\\Books\\"+name,genreId);

        try {
            bookRepo.addBook(book);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        System.out.println("after: "+file.getAbsolutePath());
        try {
            request.setAttribute("fileList", bookRepo.getAllBooks());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        request.getRequestDispatcher("/viewBooks.jsp").forward(request, response);
    }
}
