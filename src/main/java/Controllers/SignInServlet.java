package Controllers;

import Models.User;
import Repository.UserRepo;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "SignInServlet")
public class SignInServlet extends HttpServlet {
    UserRepo userRepo = new UserRepo();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String name = request.getParameter("name");
            String surname = request.getParameter("surname");
            String email = request.getParameter("email");
            String pass =  request.getParameter("password");
            User user = new User(name,surname,email,pass,2);
            userRepo.addUser(user);

            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            request.getRequestDispatcher("profileUser.jsp").forward(request,response);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
