package Controllers;

import Models.User;
import Repository.UserRepo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    private final UserRepo userRepo = new UserRepo();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                String email = request.getParameter("email");
                String pass = request.getParameter("pass");
                try {
                    User user = userRepo.autheticateUser(email, pass);
                    request.setAttribute("errorMessage", "Incorrect email or password!");
                    if (user == null) {
                        request.getRequestDispatcher("login.jsp").forward(request, response);
                    } else {
                        HttpSession session = request.getSession();
                        session.setAttribute("user", user);
                        request.getRequestDispatcher("profileUser.jsp").forward(request, response);
                    }

                } catch (SQLException throwables) {
                    throwables.printStackTrace();

            }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
