package Controllers;

import Models.Book;
import Repository.BookRepo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "SearchServlet")
public class SearchServlet extends HttpServlet {
    BookRepo bookRepo = new BookRepo();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int genre = Integer.parseInt(request.getParameter("genre"));
            String name = request.getParameter("name");
            List<Book> list = new ArrayList<>();
            if(genre == 0) {
                list.addAll(bookRepo.getBookByName(name));
            }else if(name == ""){
                list.addAll(bookRepo.getBooksByGenreId(genre));
            }else if(genre!=0 && name!=""){
                list.addAll(bookRepo.getBookByGenreAndName(name,genre));
            }
            request.setAttribute("res", list);
            request.getRequestDispatcher("resultPage.jsp").forward(request, response);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
