package Controllers;

import Models.Book;
import Models.User;
import Repository.BookRepo;
import Repository.UserRepo;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "BookOperaionsServlet")
public class BookOperaionsServlet extends HttpServlet {
    BookRepo bookRepo = new BookRepo();

    private final UserRepo userRepo = new UserRepo();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String str = request.getParameter("choice");
        try {
            Integer id  = Integer.valueOf(request.getParameter("id"));
        switch (str) {
            case ("remove"):
                bookRepo.removeBookById(id);
                request.getRequestDispatcher("/viewBooks.jsp").forward(request, response);
                 }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String str = request.getParameter("choice");
        switch (str) {
            case ("view"):
                request.getRequestDispatcher("/viewBooks.jsp").forward(request, response);
            case("upload"):
                request.getRequestDispatcher("/UploadBook.jsp").forward(request,response);
        }
    }
}
