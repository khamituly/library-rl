package Controllers;

import Models.Book;
import Models.Genre;
import Repository.BookRepo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "BookServlet")
public class BookServlet extends HttpServlet {
    BookRepo bookRepo = new BookRepo();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int id = Integer.parseInt(request.getParameter("id"));
            Book book = bookRepo.getBookById(id);
            int genreid = book.getGenreId();
            Genre genre = bookRepo.getGenreById(genreid);
            request.setAttribute("book",book);
            request.setAttribute("genre",genre);
            request.getRequestDispatcher("/book.jsp").forward(request,response);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
