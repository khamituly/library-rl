package Controllers;

import Models.Book;
import Repository.BookRepo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.SQLException;

@WebServlet(name = "DownloadServlet")
public class DownloadServlet extends HttpServlet {
    BookRepo bookRepo = new BookRepo();
    public static int BUFFER_SIZE = 1024 * 100;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        Book book = null;
        try {
            book = bookRepo.getBookById(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        File file = new File(book.getFilePath());


        String mimeType = "application/octet-stream";
        response.setContentType(mimeType);


        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", file.getName());
        response.setHeader(headerKey, headerValue);
        OutputStream outStream = null;
        FileInputStream inputStream = null;
        try {


            outStream = response.getOutputStream();
            inputStream = new FileInputStream(file);
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;


            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
        } catch (IOException ioExObj) {
            System.out.println("Exception While Performing The I/O Operation?= " + ioExObj.getMessage());
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }

            outStream.flush();
            if (outStream != null) {
                outStream.close();
            }
          }
        }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
