package Models;

import java.io.File;

public class Book {
    private int id;
    private String name;
    private String description;
    private String image;
    private String filePath;
    private int genreId;


    public Book() {
    }

    public Book(int id, String name, String description, String image) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
    }

    public Book(int id, String name, String description, String image, String filePath,int genreId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.filePath = filePath;
        this.genreId = genreId;
    }



    public Book(String name, String description, String image, String filePath,int genreId) {
        this.name = name;
        this.description = description;
        this.image = image;
        this.filePath = filePath;
        this.genreId = genreId;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
